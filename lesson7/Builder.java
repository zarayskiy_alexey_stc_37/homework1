package lesson7;

class Builder {
    public static void main(String args[]) {
        User user = User.builder()
                .firstName("Алексей")
                .lastName("Зарайский")
                .age(26)
                .isWorker(true)
                .build();
        System.out.println(user.getFirstName());
        System.out.println(user.getLastName());
        System.out.println(user.getAge());
        System.out.println(user.isWorker());
    }
}