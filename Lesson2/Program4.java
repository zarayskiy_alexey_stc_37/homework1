import java.util.Scanner;
import java.util.Arrays;

class Program4 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
        int array[] = new int [n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int min = array[0];
        int max = array[0];
        int positionOfMin = 0;
        int positionOfMax = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                positionOfMin = i;
            }

            if (array[i] > max) {
                max = array[i];
                positionOfMax = i;
            }
        }
        
        array[positionOfMin] = max;
        array[positionOfMax] = min;

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
	}
}