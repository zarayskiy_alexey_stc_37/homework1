import java.util.Arrays;

class Program5 {
	public static void main(String args[]) {
		int array[] = {84, -8, 93, 102, -678, 9463, 123, 2356, -7};
		int temp;

		for (int i = 0; i < array.length - 1; i++) {
			for (int j = array.length - 1; j > i; j--) {
				if (array[j - 1] > array[j]) {
					temp = array[j - 1];
					array[j - 1] = array[j];
					array[j] = temp;
				}
			}
		} 

			System.out.println(Arrays.toString(array));
	}
}

