class Program2 {
	public static void main (String[] args) {
		
		int number = 13452;
		
		int a1 = number % 2; // остаток 0 
		int b1 = (number / 2); // 6726
		int a2 = b1 % 2; // остаток 0
		int b2 = b1 / 2; // 3363
		int a3 = b2 % 2; // остаток 1
		int b3 = b2 / 2; // 1681
		int a4 = b3 % 2; // остаток 1
		int b4 = b3 / 2; // 840
		int a5 = b4 % 2; // остаток 0
		int b5 = b4 / 2; // 420
		int a6 = b5 % 2; // остаток 0
		int b6 = b5 / 2; // 210
		int a7 = b6 % 2; // остаток 0
		int b7 = b6 / 2; // 105
		int a8 = b7 % 2; // остаток 1
		int b8 = b7 / 2; // 52
		int a9 = b8 % 2; // остаток 0
		int b9 = b8 / 2; // 26
		int a10 = b9 % 2; // остаток 0
		int b10 = b9 / 2; // 13
		int a11 = b10 % 2; // остаток 1
		int b11 = b10 / 2; // 6
		int a12 = b11 % 2; // остаток 0
		int b12 = b11 / 2; // 3
		int a13 = b12 % 2; // остаток 1
		int b13 = b12 / 2; // 1
		
		String binaryNumber = b13 + "" + a13 + "" + a12 + "" + a11 + "" 
		+ a10+ "" + a9 + "" + a8 + "" + a7 + "" + a6 + "" + a5 
		+ "" + a4 + "" + a3 + "" + a2 + "" + a1;
		
		System.out.println (binaryNumber);
	}
}
