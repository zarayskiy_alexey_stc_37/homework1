import java.util.Arrays;
import java.util.Scanner;
class Program1 {

    public static int arraySum() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int [n];
        int arraySum = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            arraySum = arraySum + array[i];
        }
        return arraySum;
    }

    public static void reverseArray(){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int array1[] = new int[n];
        

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            array1[i] = array[array.length - 1 - i];
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array1[i] + " ");
        }   
    }

    public static double arrayAverage() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int [n];
        int arraySum = 0;
        double arrayAverage = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            arraySum = arraySum + array[i];
        }

        arrayAverage = arraySum / array.length;

        return arrayAverage;
    }

    public static void changeMinAndMax() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int [n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int min = array[0];
        int max = array[0];
        int positionOfMin = 0;
        int positionOfMax = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                positionOfMin = i;
            }

            if (array[i] > max) {
                max = array[i];
                positionOfMax = i;
            }
        }

        array[positionOfMin] = max;
        array[positionOfMax] = min;

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static void BubbleSort() {
        int array[] = {84, -8, 93, 102, -678, 9463, 123, 2356, -7};
        int temp;

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = array.length - 1; j > i; j--) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static void arrayToNumber() {
        int array[] = {4, 5 , 7, 2, 9};
        int number = 0;

        for (int i = 0; i < array.length; i++) {
            number = number * 10 + array[i];
        }
        System.out.println(number);
    }


    public static void main(String args[]) {

        System.out.println("Сумма элементов массива:" + " " + arraySum());
        System.out.println("Разворот массива:"); reverseArray();
        System.out.println("Среднее арифметическое элементов массива:" + " " + arrayAverage());
        System.out.println("Смена максимального и минимального эелементов массива:"); changeMinAndMax();
        System.out.println ();
        System.out.println("Сортировка массива методом пузырька:"); BubbleSort();
        System.out.println("Преобразование массива в число:"); arrayToNumber();
	}
}