package lesson4;

class Program2 {
    public static boolean binarySearch(int array[], int left, int right, int element) {
        int middle = left + (right - left) / 2;

        if (left <= right) {

            if (array[middle] < element) {
                return binarySearch(array, middle + 1, right, element);
            }
            else if (array[middle] > element) {
                return binarySearch(array, left, middle - 1, element);
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }

    public static void main(String args[]) {

        System.out.println(binarySearch(new int[] {2, 7, 24, 26, 79, 95, 99}, 0, 6, 100));
    }
}