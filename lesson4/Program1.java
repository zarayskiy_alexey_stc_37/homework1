package lesson4;

class Program1 {
    public static int recursion(double number) {
        if (number == 1) {
            return 1;
        }
        else if (number > 1 && number < 2) {
            return 0;
        }
        else {
            return recursion(number / 2);
        }
    }

    public static void main(String args[]) {
        double number = 23;
        System.out.println("Является ли число" + " " + number + " " + "степенью двойки?");

        if (recursion(number) == 1) {
            System.out.println("Да, является");
        }
        else {
            System.out.println("Нет, не является");
        }
    }
}